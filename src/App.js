import './App.css';
import React, {useState} from 'react';

const View = () => {
  const [plans, setPlans] = useState([])
  const [text, setText] = useState("")
  
  const Add = () => {
    setPlans(plans.concat([text]));
    setText("")
  }

  const HandleChange = (event) => {
    setText(event.target.value)
  }

  const onDelete = (index) => {
    const planCopy = plans.slice();
    planCopy.splice(index, 1)
    setPlans(planCopy)
  }

  return (<>
    <div>
      <ol>
        {plans.map((plan, index) =>
            <Item index={index} plan={plan} onDelete={onDelete}  />
          )
        }
      </ol>
      <input type="text" value={text} onChange={HandleChange} />
      <button onClick={() => Add()}>Add</button>
    </div>
  </>)
}

const Item = (props) => {
  const {index, plan, onDelete} = props
  const [checked, setChecked] = useState(false);

  return (
    <li>
        {plan}
        <input type={"checkbox"} checked={checked} onClick={() => setChecked(!checked)} />
        <button onClick={() => {
          checked && onDelete(index)
          }}>Delete</button>
    </li>
)}

const App = () => {
  return (<View />)
}

export default App;
